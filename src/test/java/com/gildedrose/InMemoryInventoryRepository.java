package com.gildedrose;

import com.gildedrose.interfaces.InventoryRepository;
import com.gildedrose.items.*;

import java.util.ArrayList;
import java.util.List;

public class InMemoryInventoryRepository implements InventoryRepository {
    private List<Item> items;
    private float balance = 0;

    public InMemoryInventoryRepository() {
        this.items = new ArrayList<>();

        items.add(new GenericItem("Rose doréé classique",15,18,10));
        items.add(new AgedItem("Aged Brie",5,15,13.4F));
        items.add(new LegendaryItem("Sulfuras",8,80,80.1F));
        items.add(new BackStageItem("Backstage passes",12,16,78));
        items.add(new ConjuredItem("Conjured product",15,30,5F));
        items.add(new RelicItem("Relic item",null,10,15));
        items.add(new AgedItem("Aged Brie2",5,15,13.4F));
        items.add(new LegendaryItem("Sulfuras 2",8,80,80.1F));
    }
    /*
    @Override
    public ArrayList<String[]> getInventory() throws IOException {

        List<String> lines = CSVHelper.readFile(CSVHelper.getResource("./data.csv"));
        ArrayList<String[]> data = new ArrayList<String[]>(lines.size());
        String sep = ";";
        for(String line : lines) {
            String[] oneData = line.split(sep);
            data.add(oneData);
        }
        System.out.println(data);
        return data;
    }*/

    @Override
    public List<Item> getInventory() {
        return this.items;
    }

    @Override
    public void saveInventory(List<Item> items) {
        this.items = items;
    }

    public void sellItem(Item itemToRemove) {
        List<Item> items = this.getInventory();

        items.remove(itemToRemove);
        this.setBalance(this.getBalance() + itemToRemove.value);

        this.saveInventory(items);
    }

    public void buyItem(Item newItem) {
        List<Item> items = this.getInventory();

        items.add(newItem);
        this.setBalance(this.getBalance() - newItem.value);

        this.saveInventory(items);
    }

    public float getBalance() {
        return this.balance;
    }

    private void setBalance(float balance) {
        this.balance = balance;
    }
}
