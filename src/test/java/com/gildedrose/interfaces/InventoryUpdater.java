package com.gildedrose.interfaces;

import java.io.IOException;

public interface InventoryUpdater {
    void UpdateQuality() throws IOException;
}
