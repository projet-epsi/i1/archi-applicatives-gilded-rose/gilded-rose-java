package com.gildedrose.interfaces;

import com.gildedrose.Item;

import java.io.IOException;
import java.util.List;

public interface InventoryViewer {
    List<Item> GetInventory() throws IOException;
}
