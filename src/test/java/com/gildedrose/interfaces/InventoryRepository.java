package com.gildedrose.interfaces;

import com.gildedrose.Item;

import java.io.IOException;
import java.util.List;

public interface InventoryRepository {
    List<Item> getInventory() throws IOException;
    void saveInventory(List<Item> items);
}
