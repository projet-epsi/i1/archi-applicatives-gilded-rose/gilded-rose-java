package com.gildedrose.items;

import com.gildedrose.Item;

public class GenericItem extends Item {
    public GenericItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }

    @Override
    public void updateQuality() {
       this.sellIn--;
       this.quality--;
    }
}
