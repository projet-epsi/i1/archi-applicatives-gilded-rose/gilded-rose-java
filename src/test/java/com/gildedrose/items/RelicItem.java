package com.gildedrose.items;

import com.gildedrose.Item;

public class RelicItem extends Item {


    public RelicItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
    }

    @Override
    public void updateQuality() {

        double new_quality = this.quality*1.05;

        if(new_quality < 100){
            this.quality = new_quality;
        }
        else{
            this.quality = 100;
        }
    }
}

