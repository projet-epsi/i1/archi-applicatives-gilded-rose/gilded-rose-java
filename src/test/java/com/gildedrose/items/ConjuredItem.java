package com.gildedrose.items;

import com.gildedrose.Item;

public class ConjuredItem extends Item {
    public ConjuredItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }

    @Override
    public void updateQuality() {
        this.sellIn--;
        this.quality = this.quality - 2;

        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}
