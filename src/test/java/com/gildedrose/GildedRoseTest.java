package com.gildedrose;

import com.gildedrose.items.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GildedRoseTest {
    InventoryInteractor interactor = new InventoryInteractor(new InMemoryInventoryRepository());
    List<String> autorizedItem = new ArrayList<>();
    Shop shop = new Shop(interactor,autorizedItem);

    @Test
    void default_item_creation() {
        String name = "Rose doréé classique";
        int sellIn = 25;
        int quality = 10;
        float value = 80.5F;

        Item item = new GenericItem(name,sellIn,quality,value);

        assertEquals(item.name, name);
        assertEquals(item.sellIn, sellIn);
        assertEquals(item.quality, quality);
    }

    @Test
    void legendary_item_creation() {
        String name = "Sulfuras";
        int sellIn = 25;
        int quality = 80;
        float value = 80.5F;

        LegendaryItem item = new LegendaryItem(name,sellIn,quality,value);

        assertEquals(item.name, name);
        assertNull(item.sellIn);
        assertEquals(quality, item.quality);
    }

    @Test
    void quality_should_be_between_0_to_50() {
        GenericItem item = new GenericItem("Rose doréé classique",20,-1,50);

        assertEquals(item.quality, 0);

        item.quality = 51;
        item.updateQuality();
        assertEquals(50, item.quality);
    }

    @Test
    void updateQuality_should_lowered_quality_by_1_for_generic_item() {
        int initialQuality = 20;
        GenericItem item = new GenericItem("Rose doréé classique",20,initialQuality,50);

        item.updateQuality();

        assertEquals(item.quality, initialQuality - 1);
    }

    @Test
    void updateQuality_should_upped_quality_by_1_for_aging_item() {
        int initialQuality = 20;
        AgedItem item = new AgedItem("Rose doréé classique",20,initialQuality,50);

        item.updateQuality();

        assertEquals(item.quality, initialQuality + 1);
    }

    @Test
    void updateQuality_should_not_change_quality_for_legendary_item() {
        int initialQuality = 80;
        LegendaryItem item = new LegendaryItem("Sulfuras",null,initialQuality,50);

        item.updateQuality();
        item.updateQuality();
        item.updateQuality();

        assertEquals(initialQuality, item.quality);
    }

    @Test
    void updateQuality_should_return_quality_of_zero_for_backstage_item_when_sellIn_lower_than_zero() {
        BackStageItem item = new BackStageItem("Backstage passes",-1,20,50);

        item.updateQuality();

        assertEquals(item.quality, 0);
    }

    @Test
    void updateQuality_should_upped_quality_by_3_for_backstage_item_when_sellIn_lower_or_equal_than_five() {
        int initialQuantity = 20;
        BackStageItem item = new BackStageItem("Backstage",5,initialQuantity,50);

        item.updateQuality();
        assertEquals(item.quality, initialQuantity + 3);

        item.sellIn = 0;
        item.updateQuality();
        assertEquals(initialQuantity + 6, item.quality);
    }

    @Test
    void updateQuality_should_upped_quality_by_2_for_backstage_item_when_sellIn_lower_or_equal_than_ten() {
        int initialQuantity = 20;
        BackStageItem item = new BackStageItem("Backstage",10,initialQuantity,50);

        item.updateQuality();
        assertEquals(item.quality, initialQuantity + 2);

        item.sellIn = 6;
        item.updateQuality();
        assertEquals(item.quality, initialQuantity + 4);
    }

    @Test
    void updateQuality_should_upped_quality_by_1_for_backstage_item_when_sellIn_upper_than_ten() {
        int initialQuantity = 20;
        BackStageItem item = new BackStageItem("Backstage",11,initialQuantity,50);

        item.updateQuality();
        assertEquals(item.quality, initialQuantity + 1);

        item.sellIn = 200;
        item.updateQuality();
        assertEquals(item.quality, initialQuantity + 2);


    }
    @Test
    void buy_item_autorized() throws IOException {

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");

        shop.buyItem(interactor.GetInventory().get(1));

        assertEquals(-13.4,Math.floor(shop.balance*100)/100);
        assertEquals(9,interactor.GetInventory().size());
    }

    @Test
    void sell_item_autorized() throws IOException {

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");

        shop.sellItem(interactor.GetInventory().get(1));

        assertEquals(13.4,Math.floor(shop.balance*100)/100);
        assertEquals(7,interactor.GetInventory().size());
    }

    @Test
    void buy_item_non_autorized() throws IOException {

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");


        assertEquals("objet non autorisé", shop.buyItem(interactor.GetInventory().get(5)));

    }

    @Test
    void sell_item_non_autorized() throws IOException {

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");


        assertEquals("objet non autorisé à la vente", shop.sellItem(interactor.GetInventory().get(5)));

    }

    @Test
    void relic_Item_not_salable() throws IOException {

        autorizedItem.add("class com.gildedrose.items.RelicItem");

        assertEquals("objet non autorisé à la vente", shop.sellItem(interactor.GetInventory().get(7)));

    }

    @Test
    void balance_update_with_relic_item () throws IOException {

        autorizedItem.add("class com.gildedrose.items.RelicItem");
        shop.buyItem(interactor.GetInventory().get(7));
        shop.add_relic_value();



        assertEquals(0.15,Math.floor(shop.balance*100)/100);
    }

}
