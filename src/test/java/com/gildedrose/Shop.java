package com.gildedrose;

import com.gildedrose.items.RelicItem;

import java.io.IOException;
import java.util.List;

public class Shop {

    private InventoryInteractor repository;
    public List<String> autorized_items;
    public float balance = 0;

    public Shop(InventoryInteractor repository, List<String> autorized_items){
        this.repository = repository;
        this.autorized_items = autorized_items;

    }

    public void add_relic_value() throws IOException {
        for(Item i : repository.GetInventory()){
            if(i.getClass().toString().equals("class com.gildedrose.items.RelicItem")){
                this.balance = (float) Math.floor((this.balance + (i.quality/1000)*i.value)*100)/100;
            }
        }
    }

    public String sellItem(Item item) throws IOException {
        if (autorized_items.contains(item.getClass().toString()) && !item.getClass().toString().equals("class com.gildedrose.items.RelicItem")) {
            this.balance = this.balance + item.value;
            repository.GetInventory().remove(item);
            return String.valueOf(balance);
        }
        else{
            return "objet non autorisé à la vente";
        }

    }

    public String buyItem(Item item) throws IOException {
        if (autorized_items.contains(item.getClass().toString())) {
            this.balance = this.balance - item.value;
            repository.GetInventory().add(item);
            return String.valueOf(balance);
        }
        else   {
            return "objet non autorisé";
        }

    }
}
