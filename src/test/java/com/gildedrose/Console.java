package com.gildedrose;

import com.gildedrose.interfaces.InventoryUpdater;
import com.gildedrose.interfaces.InventoryViewer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Console {
    public static void main(String[] args) throws IOException {
        InventoryInteractor interactor = new InventoryInteractor(new InMemoryInventoryRepository());


        InventoryUpdater inventoryUpdater = interactor;
        inventoryUpdater.UpdateQuality();

        InventoryViewer inventoryViewer = interactor;
        for (Item i : inventoryViewer.GetInventory()){
            System.out.println(i.name);
        }

        List<String> autorizedItem = new ArrayList<>();
        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");
        autorizedItem.add("class com.gildedrose.items.RelicItem");


        Shop shop = new Shop(interactor, autorizedItem);

        System.out.println(shop.buyItem(inventoryViewer.GetInventory().get(7)));

        System.out.println(shop.sellItem(inventoryViewer.GetInventory().get(7)));

        Transmuter transmutation = new Transmuter();

        //transmutation.Transmute_item(shop,interactor.GetInventory().get(7));

        shop.add_relic_value();
        System.out.println(shop.balance);

        for (Item i : inventoryViewer.GetInventory()){
            System.out.println(i.name);

        }
    }
}
